#!/bin/bash


video="$1"


function die
{
	echo "$@"
	exit 1
}


if [[ ! -f "$video" ]] ; then
	die "Need video file"
fi


img_dir="${video}.img"
log_file="$img_dir/output.log"

rm -rf "$img_dir"
mkdir -p "$img_dir"

# https://stackoverflow.com/questions/34786669/extract-all-video-frames-as-images-with-ffmpeg/34786929
echo "Will extract frames from '$video' to folder '$img_dir'"
ffmpeg -i "${video}" "$img_dir/frame-%04d.jpg" &> "$log_file"
echo "Done"

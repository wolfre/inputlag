#!/bin/bash

fps=$1
frames=$2

if [[ "$fps" == "" ]] || [[ "$frames" == "" ]] ; then
	echo "need <fps> and <frame count>"
	exit 1
fi

time_ms=$(( $frames * 1000 ))
time_ms=$(( $time_ms / $fps ))

echo "$frames frames at $fps fps, are $time_ms ms"

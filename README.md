# Measuring the input lag on my Panasonic TX-50EXW784 TV & Marantz SR6008 receiver
 
## Setup HW

- TV Panasonic TX-50EXW784
  - Note that this specific TV cannot switch aspect ratios in *game mode*, and will always stretch the image to 16:9.
    It can do so in non-game mode however.
    The aspect ratio menu in game mode is reduced to just one entry (16:9) so this seems to be a limitation by the SW and not a bug.
    This limitation is quite arbitrary and results in the wrong aspect ratio in most games prior to the PS3 / XBox 360 generation (as they were generally created for 4:3 displays).
    This limitation is the reason for this experiment as the receiver is able to do aspect ratio correction / scaling in non-game modes.
- Receiver (RX) Marantz SR6008
- PAL Wii, Ver. 4.3E
- One Plus Nord with android 10, stock camera app
  - 240 frames per sec slow motion camera (see [results](#results))

## Method

Setup camera to view wii remote and tv at the same time.
Wii is connected to RX via component cables.
Tap wii mote with a fast moving pencil to trigger motion.
Count pictures between pencil impact and reaction on screen.

This will measure the complete input lag of the wii (sensor system + software), TV and RX.
With this method no absolute input lag values can be measured.
However the relative comparison between the various settings can.
As a baseline, TV in game mode and RX in game mode feel responsive and pleasant in wii system menu (subjective observation by author).
Setting this TV to normal mode yields considerable lag, so high that any measurements were not considered.
Each test was done once (unless noted otherwise), so the results are only somewhat reliable (see [conclusion](#conclusion)).

## Results

For all tests, the TV was in game mode.
For all results:
- the "hit" frame is the first frame with visible displacement of the Wii remove
- the "react" frame is the first frame with visible displacement of cursor (hand) on screen
- the frame numbers refer to the absolute frame numbers in the original videos
- results with two frame numbers for "hit" or "react" had captured a very slight movement on the first frame and a clear one on the second.

Timing the One Plus Nord Camera:
- A stop watch (on another phone) was filmed
  - 1 second was counted to start at frame 756 and end at frame 993
  - 1 second was counted as 237 frames which fits the phone spec sheet of 240fps
  - All calculated times below were done with 240fps.
    As the filmed stop watch was running with 60fps sticking to the spec value of 240 fps was considered more accurate.

Measuring the input lag, the settings here refer to the configurations in "Video/Output Settings" menu on the RX:
- "Video Mode" **Game**.
  Any scaling / deinterlacing seems to be disabled in the RX and it will just digitize the video.
  This was determined by looking at the signal info of the TV (which was the same as what the Wii output).
  - Wii **480p60** lag **16 frames / 66 ms**
    - hit: 634
    - react: 650
  - Wii **480i60** lag **26-27 frames / 108-112 ms**
    - hit: 817
    - react: 843-844
  - Wii **576i50** lag **31-32 frames / 129-133 ms**
    - hit: 568
    - react: 599-600
- "Video Mode" **Movie**, 
  "Progressive Mode" **Video**, 
  "Resolution (HDMI)" **1080p**, 
  "Aspec Ratio" **16:9**
  - Wii **480p60** (run 1) lag **21 frames / 87 ms**
    - hit: 415
    - react: 436
  - Wii **480p60** (run 2) lag **18-19 frames / 75-79 ms**
    - hit: 513
    - react: 531-532
  - Wii **480p60** (run 3) lag **19 frames / 79 ms**
    - hit: 364
    - react: 383
  - Wii **480i60** lag **26 frames / 108 ms**
    - hit: 657
    - react: 683
  - Wii **576i50** lag **29-30 frames / 120-125 ms**
    - hit: 498
    - react: 527-528
- "Video Mode" **Movie**,
  "Progressive Mode" **Video & Film**,
  "Resolution (HDMI)" **1080p**, 
  "Aspec Ratio" **16:9**
  - Wii **480i60** lag **23 frames / 95 ms**
    - hit: 308
    - react: 331
- "Video Mode" **Movie**,
  "Progressive Mode" **Video**,
  "Resolution (HDMI)" **1080p**,
  "Aspec Ratio" **4:3**
  - Wii **480p60** lag **21 frames / 87 ms**
    - hit: 414
    - react: 435
- "Video Mode" **Movie**,
  "Progressive Mode" **Video**,
  "Resolution (HDMI)" **720p**,
  "Aspec Ratio" **16:9**
  - Wii **480p60** lag **26 frames / 108 ms**
    - hit: 502
    - react: 528
- "Video Mode" **Movie**,
  "Progressive Mode" **Video**,
  "Resolution (HDMI)" **480p**,
  "Aspec Ratio" **16:9**
  - Wii **480p60** lag **18 frames / 75 ms**
    - hit: 351
    - react: 369
- "Video Mode" **Movie**,
  "Progressive Mode" **Video & Film**,
  "Resolution (HDMI)" **480p**,
  "Aspec Ratio" **16:9**
  - Wii **480p60** lag **18-19 frames / 75-79 ms**
    - hit: 189
    - react: 207-208

When the TV is set to game mode, most options are disabled.
One that is not, is called "1080p Pixel by 4pixels".
All previous measurements were done with this set to **off**.
Setting it to **on** yields a slightly softer image.
Results with this option set to **on**:
- "Video Mode" **Movie**, 
  "Progressive Mode" **Video**, 
  "Resolution (HDMI)" **1080p**, 
  "Aspec Ratio" **16:9**
  - Wii **480p60** (run 1) lag **21-22 frames / 87-91 ms**
    - hit: 237
    - react: 258-259
  - Wii **480p60** (run 2) lag **22 frames / 91 ms**
    - hit: 293
    - react: 315
    
## Conclusion

Using the wii system menu pointer to measure input lag will certainly add quite a bit to the delay.
This comes from processing the wii remote camera image and the transmission chain via Bluetooth.
As expected setting both TV and RX into game mode provides the lowest lag with about 66ms.

Lets look at the 480p60 results in more details:
- "Video Mode" **Game**, lag **16 frames / 66 ms**
- "Video Mode" **Movie**, "Progressive Mode" **Video**, "Resolution (HDMI)" **1080p**, "Aspec Ratio" **16:9**, lag **20 frames / 83 ms**
- "Video Mode" **Movie**, "Progressive Mode" **Video**, "Resolution (HDMI)" **1080p**, "Aspec Ratio" **4:3**, lag **21 frames / 87 ms**
- "Video Mode" **Movie**, "Progressive Mode" **Video**, "Resolution (HDMI)" **720p**, "Aspec Ratio" **16:9**, lag **26 frames / 108 ms**
- "Video Mode" **Movie**, "Progressive Mode" **Video**, "Resolution (HDMI)" **480p**, "Aspec Ratio" **16:9**, lag **18 frames / 75 ms**
- "Video Mode" **Movie**, "Progressive Mode" **Video & Film**, "Resolution (HDMI)" **480p**, "Aspec Ratio" **16:9**, lag **18 frames / 75 ms**

The "Progressive Mode" switch, which probably configures the deinterlacing algorithm used, does not make a difference.
The output resolution however does: setting it to 480p yields almost the same low lag regardless of the "Video Mode" setting.
This could be due to some scaling stage in the RX can be skipped, it is just digitizing the input.
Further more an output resolution of 720p has quite a bit more lag than 1080p.
This could be due to uneven scaling from the input 480p to 720p in the RX. 
However this is also true for 1080p output, so this is more likely something in the TV.
The TV has a 4k panel, so it can evenly scale 1080p to its pixels.
Scaling a 720p input signal may be scaled using a different algorithm than a 1080p signal.
Finally the aspect ratio does not impact the lag, and neither does the TV setting "1080p Pixel by 4pixels".

As an additional step I set the aspect ratio to 4:3 for a 480p output from the RX.
This does nothing compared to 16:9, and the image on the TV appears stretched.
The TV ignores any meta info sent via HDMI here (assuming it is sent by the RX).
For 720p and 1080p outputs the RX will window box the image.
This means it actually sends a 16:9 image, with the active 4:3 image area padded by black bars left and right to fill 16:9.
This window boxing just described is still conjecture, and needs verifying.

Next lets compare 480i60 results:
- "Video Mode" **Game**, lag **26 frames / 108 ms**
- "Video Mode" **Movie**, "Progressive Mode" **Video**, "Resolution (HDMI)" **1080p**, "Aspec Ratio" **16:9**, lag **26 frames / 108 ms**
- "Video Mode" **Movie**, "Progressive Mode" **Video & Film**, "Resolution (HDMI)" **1080p**, "Aspec Ratio" **16:9**, lag **23 frames / 95 ms**

So the results are close and each test was performed only once.
Judging from the only test performed 3 times (see [results](#results)) the error is probably around 2~3 frames.
With that in mind the "Progressive Mode" set to "Video & Film" seems to be a bit faster.
Note that in "Game" mode the receiver passes interlaced video to the TV, so deinterlacing is done by the TV.
Now comparing a lag of 103 ms (the average of the results) to a lag of 83 ms (480p60 -> 1080p result) shows an additional 20ms lag.
Just for reference, at 60 frames/field per second, one frame/field is 17ms.
Considering the measurement error the additional delay could be exactly one field (17 ms).
This may be needed by the deinterlacing algorithm to do its job.
Looking at the output of the deinterlacer no visible edge flickering could be seen.
This points to a more complex algorithm than a simple bob deinterlacer, which would have introduced no additional lag.

Finally lets look at the 576i50 results:
- "Video Mode" **Movie**, "Progressive Mode" **Video**, "Resolution (HDMI)" **1080p**, "Aspec Ratio" **16:9**
  - **480p60** lag **20 frames / 83 ms**
  - **480i60** lag **26 frames / 108 ms**
  - **576i50** lag **29 frames / 120 ms**

The lag in PAL i50 is higher than NTSC i60 timing.
Lets assume the deinterlacing algorithm is the same in for both TV systems.
This is a reasonable assumption as deinterlacing techniques are not known for being affected by refresh rate.
From the above conclusion on 480p vs 480i, we can also reasonably assume the algorithm buffers at least two fields.
A PAL field is 20 ms, compared the 17 ms from NTSC, so with longer field times comes longer processing time, and higher lag.
Due to the high margin of error comparing the exact numbers from the i50 and i60 measurements is not possible.

# Measuring the input lag on my Panasonic TX-50EXW784 TV

## Setup HW

The basic setup is the same as with previous tests, but without the RX in the chain.
This means the component outputs from the Wii were directly connected the respective inputs on the TV.

## Method

The method is the same as with previous tests.
However during testing the additional Wii menu for sensor bar calibration was closer investigated.
Here one can see a direct image of what the camera in the Wii remote sees (two bright dots, the LEDs in the sensor bar).
The input lag was noted to be significantly lower than with the regular menu pointer.
So one additional test was performed with this screen, using the same "hit" to "react" frame counting method.

## Results

Tests done using the pointer in the home screen:
- "Game mode" **on**
  - Wii **480p60** lag **20 frames / 83 ms**
    - hit: 317
    - react: 337
  - Wii **480i60** lag **25 frames / 104 ms**
    - hit: 183
    - react: 208
  - Wii **576i50** lag **33 frames / 137 ms**
    - hit: 157
    - react: 190

Tests done in the sensor bar calibration screen.
- "Game mode" **on**
  - Wii **480p60** lag **12 frames / 50 ms**
    - hit: 312
    - react: 324

## Conclusion

Testing the TV without the RX it is hard to directly compare individual aspects.
In this setup the TV has to do additional processing (which was previously done in the RX) compared to an HDMI input.
However the overall performance (input lag) can of course be compared.

Looking at the 480p60 results there is a clear difference:
- TV only, lag **20 frames / 83 ms**
- With RX in "Video Mode" **Game**, lag **16 frames / 66 ms**
- With RX in "Video Mode" **Movie**, "Progressive Mode" **Video**, "Resolution (HDMI)" **1080p**, "Aspec Ratio" **16:9**, lag **20 frames / 83 ms**

The analog to digital subsystem in the TV does seem to be slower compared to the RX doing the same job in game mode.
The lag is on par with the slower RX "Video Mode" **Movie** where scaling correct aspect ratio is possible.

For interlaced content both setups perform with similar lag:
- Wii **480i60**
  - TV only, lag **25 frames / 104 ms**
  - With RX in "Video Mode" **Game**, lag **26 frames / 108 ms**
- Wii **576i50**
  - TV only, lag **33 frames / 137 ms**
  - With RX in "Video Mode" **Game**, lag **32 frames / 133 ms**

The final test done with the sensor bar calibration screen gave some additional hints on the SW processing delay in the Wii:
- "Game mode" **on**, Wii **480p60** 
  - Sensor bar screen, lag **12 frames / 50 ms**
  - Wii home menu, lag **20 frames / 83 ms**

Comparing the sensor bar screen lag with the home menu showes that there is an additional 7 to 8 frames (29-33 ms) lag (in the home screen).
Getting correct 4:3 aspect ratio on this setup was the initial starting point.
Looking again at the previous result when scaling to 4:3 aspect ratio:
- "Video Mode" **Movie**, "Progressive Mode" **Video**, "Resolution (HDMI)" **1080p**, "Aspec Ratio" **4:3**, lag **21 frames / 87 ms**

Form the achieved 87 ms lag, about 30 ms can be attributed to the system menu (software).
So the real input lag is closer to 57 ms which is about 3 to 4 frames at 480p60.

# Outlook

Using a better setup should give accurate results:
- oscilloscope + light probe + tap on the analog video cable
